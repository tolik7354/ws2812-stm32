
#ifdef STM32F031
	#include "stm32f0xx.h"
#endif
#ifdef STM32F407
	#include "stm32f4xx.h"
#endif

#include "WS_2812.h"

/*
TO DO:

	// select RCC on depends TIM and DMA and DMA1_Channel1_IRQHandler
*/

/*
	
	Issues: if change colours fast then leds are working incorrect

	better managment: LOW VALUE ON OUTPUT TIMER IS BETTER
*/

//void sys_clock_config (void);
//void gpio_config (void);

int main(void) {
//	sys_clock_config();
//	gpio_config();	
//	
//	init_ws2812(TIM17);
//	
////	test_SK6812RGBW(2, 1, 1, 1, 1);
//	
//	for (uint8_t i = 0; i < COUNT_LEDS; i++) {
//		test_SK6812RGBW(i, 255, 255, 255, 255);
//	}
	
	// 1
	
//	test_SK6812RGBW(0, 0, 0, 0, 1);
//	test_SK6812RGBW(1, 0, 0, 0, 0);
//	test_SK6812RGBW(2, 0, 0, 0, 0);
//	
//	test_SK6812RGBW(3, 0, 0, 0, 1);
//	test_SK6812RGBW(4, 0, 0, 0, 0);
//	test_SK6812RGBW(5, 0, 0, 0, 0);
//	
//	test_SK6812RGBW(6, 0, 0, 0, 1);
//	test_SK6812RGBW(7, 0, 0, 0, 0);
//	test_SK6812RGBW(8, 0, 0, 0, 0);
//	
//	test_SK6812RGBW(9, 0, 0, 0, 1);
//	test_SK6812RGBW(10, 0, 0, 0, 0);
//	test_SK6812RGBW(11, 0, 0, 0, 0);
//	
//	test_SK6812RGBW(12, 0, 0, 0, 1);
//	test_SK6812RGBW(13, 0, 0, 0, 0);
//	test_SK6812RGBW(14, 0, 0, 0, 0);
//	
//	// 5
//	
//	test_SK6812RGBW(0, 0, 0, 0, 1);
//	test_SK6812RGBW(1, 0, 0, 0, 1);
//	test_SK6812RGBW(2, 0, 0, 0, 1);
//	
//	test_SK6812RGBW(3, 0, 0, 0, 0);
//	test_SK6812RGBW(4, 0, 0, 0, 0);
//	test_SK6812RGBW(5, 0, 0, 0, 1);
//	
//	test_SK6812RGBW(6, 0, 0, 0, 1);
//	test_SK6812RGBW(7, 0, 0, 0, 1);
//	test_SK6812RGBW(8, 0, 0, 0, 1);
//	
//	test_SK6812RGBW(9, 0, 0, 0, 1);
//	test_SK6812RGBW(10, 0, 0, 0, 0);
//	test_SK6812RGBW(11, 0, 0, 0, 0);
//	
//	test_SK6812RGBW(12, 0, 0, 0, 1);
//	test_SK6812RGBW(13, 0, 0, 0, 1);
//	test_SK6812RGBW(14, 0, 0, 0, 1);
//	
//	// 2
//	
//	test_SK6812RGBW(0, 0, 0, 0, 1);
//	test_SK6812RGBW(1, 0, 0, 0, 1);
//	test_SK6812RGBW(2, 0, 0, 0, 1);
//	
//	test_SK6812RGBW(3, 0, 0, 0, 1);
//	test_SK6812RGBW(4, 0, 0, 0, 0);
//	test_SK6812RGBW(5, 0, 0, 0, 0);
//	
//	test_SK6812RGBW(6, 0, 0, 0, 1);
//	test_SK6812RGBW(7, 0, 0, 0, 1);
//	test_SK6812RGBW(8, 0, 0, 0, 1);
//	
//	test_SK6812RGBW(9, 0, 0, 0, 0);
//	test_SK6812RGBW(10, 0, 0, 0, 0);
//	test_SK6812RGBW(11, 0, 0, 0, 1);
//	
//	test_SK6812RGBW(12, 0, 0, 0, 1);
//	test_SK6812RGBW(13, 0, 0, 0, 1);
//	test_SK6812RGBW(14, 0, 0, 0, 1);
	
	
	
		//test_SK6812RGBW(15, 1, 1, 1, 1);
	//test_SK6812RGBW(1, 1, 1, 1, 1);
	
	//test_SK6812RGBW(2, 1, 1, 1, 1);
	/*
	set_colour_one(0, 0, 255, 0);
	set_colour_one(1, 0, 255, 0);
	set_colour_one(2, 0, 0, 255);
	set_colour_one(3, 255, 255, 255);

	set_colour_one(0, 0, 0, 255);
	set_colour_one(0, 0, 0, 0);
	set_colour_one(1, 0, 255, 255);
	set_colour_one(2, 255, 0, 255);
	set_colour_one(3, 127, 127, 127);

	set_colour_one(0, 10, 0, 0);
	set_colour_one(1, 0, 1, 0);
	set_colour_one(2, 0, 0, 1);
	set_colour_one(3, 1, 1, 1);

	set_colour_one(0, 0, 255, 0);

	set_colour_range(1, 2, 50, 50, 0);
	
	set_colour_one(0, 255, 0, 0);
	set_colour_one(1, 127, 0, 0);
	set_colour_one(2, 60, 0, 0);
	set_colour_one(3, 30, 0, 0);
	
	set_colour_one(0, 255, 0, 0);
	set_colour_one(1, 255, 0, 0);
	set_colour_one(2, 255, 0, 0);
	set_colour_one(3, 255, 0, 0);

	set_colour_one(1, 0, 0, 255);
	set_colour_one(2, 0, 0, 255);

	for (uint8_t i = 0; i < COUNT_LEDS; i++) {
		if (i % 2 == 0) {
			set_colour_one(i, 30, 0, 30);
		}
		else {
			set_colour_one(i, 0, 30, 0);
		}
	}
	
	for (uint32_t i = 0; i < 0x000FFFFF; i++) {
		__asm("nop");
	}
	
	for (uint8_t i = 0; i < COUNT_LEDS; i++) {
		if (i % 2 == 0) {
			set_colour_one(i, 0, 30, 0);
		}
		else {
			set_colour_one(i, 30, 0, 30);
		}
	}	
	
	for (uint32_t i = 0; i < 0x000FFFFF; i++) {
		__asm("nop");
	}
	*/
  while (1) {	
/*
		for (uint8_t i = 0; i < COUNT_LEDS; i++) {
			if (i % 2 == 0) {
				set_colour_one(i, 30, 0, 30);
			}
			else {
				set_colour_one(i, 0, 30, 0);
			}
		}
		
		for (uint32_t i = 0; i < 0x00FFFFFF; i++) {
			__asm("nop");
		}
		
		for (uint8_t i = 0; i < COUNT_LEDS; i++) {
			if (i % 2 == 0) {
				set_colour_one(i, 0, 30, 0);
			}
			else {
				set_colour_one(i, 30, 0, 30);
			}
		}	
		
		for (uint32_t i = 0; i < 0x00FFFFFF; i++) {
			__asm("nop");
		}
		*/
  }
}

//void gpio_config (void) {
//	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
//	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
//	
//	// PA13 and PA14 init
//	GPIOA->MODER |= GPIO_MODER_MODER13_1 | GPIO_MODER_MODER14_1;
//	GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR13_0 | GPIO_OSPEEDR_OSPEEDR13_1;	// high speed
//	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR13_0 | GPIO_PUPDR_PUPDR14_1;
//	
//	// PA7 TIM17 ch1
//	GPIOA->AFR[0] |= (uint32_t)((0x05) << 28);	// select AF5 (TIM17 ch1)
//	
//	//GPIOA->OTYPER	|= GPIO_OTYPER_OT_7;		// output open-drain (1)
//	//GPIOA->PUPDR	|= GPIO_PUPDR_PUPDR7_0;		// Pull-up (01)
//	//GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR7_0 | GPIO_OSPEEDR_OSPEEDR7_1;
//	
//	GPIOA->MODER |= GPIO_MODER_MODER7_1;	// alternative func (10)
//}

//void sys_clock_config (void) {
//		RCC->CR |= RCC_CR_HSION;	// HSI clock enable (bit 0 to 1)

//		while ((RCC->CR & RCC_CR_HSIRDY) != RCC_CR_HSIRDY) {}	// wait when HSI stay en
//		
//		RCC->CFGR |= RCC_CFGR_PLLNODIV;	
//			
//		//RCC->CFGR |= RCC_CFGR_MCO_0 | RCC_CFGR_MCO_1 | RCC_CFGR_MCO_2;	//  PLL selected
//		RCC->CFGR |= RCC_CFGR_SW_PLL | RCC_CFGR_PLLMULL12 | RCC_CFGR_PLLSRC_HSI_DIV2;	//	PLL * 12
//		//RCC->CFGR |= RCC_CFGR_PLLSRC_1; //	HSI selected as PLL input clock (bit 15 not available for f031)

//		RCC->CR |= RCC_CR_PLLON;
//			
//		while ((RCC->CR & RCC_CR_PLLRDY) != RCC_CR_PLLRDY) {}	// wait when PLL stay en	
//}
