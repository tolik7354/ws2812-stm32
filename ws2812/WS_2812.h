#ifdef STM32F031
	#include "stm32f0xx.h"
#endif
#ifdef STM32F407
	#define STM32F40_41xxx
	#include "stm32f4xx.h"
#endif
#ifdef WS2812_SETTINGS

	#define COUNT_LEDS	(uint8_t)15 //8
	
	#define ONE_LED_SIZE	(uint8_t)32	// 24
	#define RESET_SIZE		(uint8_t)80 // 50
	
	#define BUF_LEDS_SIZE	(uint16_t)(COUNT_LEDS * ONE_LED_SIZE + RESET_SIZE)
	
	#define PEPIOD		14
	#define PRESCALER	3
//	
	#define ZERO_BIT	3 // 5
	#define ONE_BIT	7 // 9
//	
//	#define ZERO_BIT	9
//	#define ONE_BIT	5
	/*
	RESET_BEFORE_DATA works better I did not see artifact on the last led like in case RESET_AFTER_DATA
	
	Not so
	
	RESET_AFTER_DATA better because if RESET_BEFORE_DATA will be that first led in the first send data will green every time
	
	*/
	
	//#define RESET_BEFORE_DATA 
	#define RESET_AFTER_DATA
	
#endif 

struct smart_led_dma {
	GPIO_TypeDef gpio;
	uint16_t pin;
	TIM_TypeDef	tim;
	uint8_t tim_ch;
	DMA_TypeDef dma;
	uint8_t dma_ch;
};

void init_smart_led_dma (GPIO_TypeDef gpio_led, uint16_t pin_led, TIM_TypeDef tim_led, uint8_t tim_ch_led, DMA_TypeDef dma_led, uint8_t dma_ch_led);


void init_ws2812 (TIM_TypeDef* TIM_WS2812);
void set_colour_one(uint8_t num_led, uint8_t _green, uint8_t _red, uint8_t _blue);
void set_colour_range(uint8_t st_led, uint8_t end_led, uint8_t _green, uint8_t _red, uint8_t _blue);

void test_SK6812RGBW (uint8_t num_led, uint8_t _green, uint8_t _red, uint8_t _blue, uint8_t white);
