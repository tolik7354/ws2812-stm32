#ifndef WS_2812_H
#define WS_2812_H

#include "WS_2812.h"

uint16_t buf_leds[BUF_LEDS_SIZE] = {0};

volatile uint8_t dma_lock = 0;

//static DMA_Channel_TypeDef* DMA_Channel_WS_2812;

struct smart_led_dma smart_led;

void init_smart_led_dma (GPIO_TypeDef gpio_led, uint16_t pin_led, TIM_TypeDef tim_led, uint8_t tim_ch_led, DMA_TypeDef dma_led, uint8_t dma_ch_led) {
	smart_led.gpio = gpio_led;
	smart_led.pin = pin_led;
	smart_led.tim = tim_led;
	smart_led.tim_ch = tim_ch_led;
	smart_led.dma = dma_led;
	smart_led.dma_ch = dma_ch_led;
}


//void test_SK6812RGBW (uint8_t num_led, uint8_t _green, uint8_t _red, uint8_t _blue, uint8_t white) {
//	while (dma_lock == 1) {}
//	dma_lock = 1;
//	if (num_led <= COUNT_LEDS) {
//		//while ((DMA1->ISR & DMA_CCR_TCIE) == DMA_CCR_TCIE);
//		NVIC_DisableIRQ(DMA1_Channel1_IRQn);
//		DMA1_Channel1->CCR &= (0<<0);

//		// set green 
//		for (uint8_t i = 0; i < 8; i++) {
//			uint8_t temp = (_green >> i) & 0x01;
//		
//	#ifdef RESET_BEFORE_DATA
//			if (temp == 0) {buf_leds[(7 - i) + (num_led) * ONE_LED_SIZE + RESET_SIZE] = ZERO_BIT;}
//			else {					buf_leds[(7 - i) + (num_led) * ONE_LED_SIZE + RESET_SIZE] = ONE_BIT;}
//	#endif
//			
//	#ifdef RESET_AFTER_DATA
//			if (temp == 0) {buf_leds[(7 - i) + (num_led) * ONE_LED_SIZE] = ZERO_BIT;}
//			else {					buf_leds[(7 - i) + (num_led) * ONE_LED_SIZE] = ONE_BIT;}
//	#endif
//		}
//		
//		// set red 
//		for (uint8_t i = 0; i < 8; i++) {
//			uint8_t temp = (_red >> i) & 0x01;
//			
//	#ifdef RESET_BEFORE_DATA
//			if (temp == 0) {buf_leds[(15 - i) + (num_led ) * ONE_LED_SIZE + RESET_SIZE] = ZERO_BIT;}
//			else {					buf_leds[(15 - i) + (num_led) * ONE_LED_SIZE + RESET_SIZE] = ONE_BIT;}
//	#endif
//			
//	#ifdef RESET_AFTER_DATA
//			if (temp == 0) {buf_leds[(15 - i) + (num_led) * ONE_LED_SIZE] = ZERO_BIT;}
//			else {					buf_leds[(15 - i) + (num_led) * ONE_LED_SIZE] = ONE_BIT;}
//	#endif
//		}
//		
//		// set blue 
//		for (uint8_t i = 0; i < 8; i++) {
//			uint8_t temp = (_blue >> i) & 0x01;
//			
//	#ifdef RESET_BEFORE_DATA
//			if (temp == 0) {buf_leds[(23 - i) + (num_led) * ONE_LED_SIZE + RESET_SIZE] = ZERO_BIT;}
//			else {					buf_leds[(23 - i) + (num_led) * ONE_LED_SIZE + RESET_SIZE] = ONE_BIT;}
//	#endif
//			
//	#ifdef RESET_AFTER_DATA
//			if (temp == 0) {buf_leds[(23 - i) + (num_led) * ONE_LED_SIZE] = ZERO_BIT;}
//			else {					buf_leds[(23 - i) + (num_led) * ONE_LED_SIZE] = ONE_BIT;}
//	#endif	
//			
//			
//		// set white 
//		for (uint8_t i = 0; i < 8; i++) {
//			uint8_t temp = (white >> i) & 0x01;
//			
//	#ifdef RESET_BEFORE_DATA
//			if (temp == 0) {buf_leds[(31 - i) + (num_led) * ONE_LED_SIZE + RESET_SIZE] = ZERO_BIT;}
//			else {					buf_leds[(31 - i) + (num_led) * ONE_LED_SIZE + RESET_SIZE] = ONE_BIT;}
//	#endif
//			
//	#ifdef RESET_AFTER_DATA
//			if (temp == 0) {buf_leds[(31 - i) + (num_led) * ONE_LED_SIZE] = ZERO_BIT;}
//			else {					buf_leds[(31 - i) + (num_led) * ONE_LED_SIZE] = ONE_BIT;}
//	#endif	
//		}			
//			
//		}
//		
//		 //buf_leds[BUF_LEDS_SIZE - 1] = PEPIOD + 1;
//		//buf_leds[BUF_LEDS_SIZE - 1] = 0;
//		
//		DMA1_Channel1->CPAR = (uint32_t) (&(TIM17->CCR1));
//		DMA1_Channel1->CMAR = (uint32_t)(&buf_leds[0]);
//		DMA1_Channel1->CNDTR = BUF_LEDS_SIZE;
//		//DMA1_Channel1->CNDTR = BUF_LEDS_SIZE + 1;
//		//DMA1_Channel1->CNDTR = BUF_LEDS_SIZE - 1;
//		
//		DMA1_Channel1->CCR |= DMA_CCR_DIR | DMA_CCR_MINC | 
//													DMA_CCR_TEIE | DMA_CCR_TCIE ;

//		DMA1_Channel1->CCR |= DMA_CCR_MSIZE_0;
//		DMA1_Channel1->CCR |= DMA_CCR_PSIZE_0;
//		
//		DMA1_Channel1->CCR |= DMA_CCR_EN;
//		
//		NVIC_EnableIRQ(DMA1_Channel1_IRQn);
//		NVIC_SetPriority(DMA1_Channel1_IRQn, 0);
//		
//	}

//}

//void init_ws2812 (TIM_TypeDef* TIM_WS2812) {

//	RCC->AHBENR |= RCC_AHBENR_DMA1EN;
//	RCC->APB2ENR |= RCC_APB2ENR_TIM17EN;
//	
//	TIM_WS2812->CCMR1 |= TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1PE;	// mode 1
//	
//	TIM_WS2812->DIER |= TIM_DIER_UDE;
//		
//	TIM_WS2812->CR1 |= TIM_CR1_ARPE;
//	
//	TIM_WS2812->CCER |= TIM_CCER_CC1E;
//	//TIM_WS2812->CCER |= TIM_CCER_CC1P; // polarity low
//	
//	TIM_WS2812->PSC = (uint32_t)(PRESCALER); // frequency 12 MHz
//	
//	TIM_WS2812->ARR = (uint32_t)(PEPIOD);
//	
//	//TIM_WS2812->CCR1 = (uint32_t)(PEPIOD + 1);
//	TIM_WS2812->CCR1 = 0;
//	
//	TIM_WS2812->BDTR |= TIM_BDTR_MOE;
//	TIM_WS2812->CR1 |= TIM_CR1_CEN;	// timer on	
//	/*
//	TIM_WS2812->CCMR1 |= TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1PE;	// mode 1
//	
//	TIM_WS2812->DIER |= TIM_DIER_UDE;
//		
//	TIM_WS2812->CR1 |= TIM_CR1_ARPE;
//	
//	TIM_WS2812->CCER |= TIM_CCER_CC1E;
//	// TIM17->CCER |= TIM_CCER_CC1P; // polarity low
//	
//	TIM_WS2812->PSC = (uint32_t)(PRESCALER); // frequency 12 MHz
//	
//	TIM_WS2812->ARR = (uint32_t)(PEPIOD);
//	
//	TIM_WS2812->CCR1 = (uint32_t)(PEPIOD + 1);
//	
//	TIM_WS2812->BDTR |= TIM_BDTR_MOE;
//	TIM_WS2812->CR1 |= TIM_CR1_CEN;	// timer on
//	*/
//	#ifdef RESET_BEFORE_DATA
//		for (uint16_t i = 0; i < RESET_SIZE; i++) { buf_leds[i] = PEPIOD + 1; }
//		for (uint16_t i = RESET_SIZE; i < BUF_LEDS_SIZE; i++) { buf_leds[i] = ZERO_BIT; }
//	#endif
//			
//	#ifdef RESET_AFTER_DATA	
//		for (uint16_t i = 0; i < BUF_LEDS_SIZE - RESET_SIZE; i++) { buf_leds[i] = ZERO_BIT; }
//		for (uint16_t i = BUF_LEDS_SIZE - RESET_SIZE; i < BUF_LEDS_SIZE; i++) { 
//			buf_leds[i] = 0; 
//			//buf_leds[i] = PEPIOD + 1;
//		}
//	#endif	
//}

///*
//  * @num_leds		-> 		number of led in line number starts from 0 to COUNT_LEDS - 1
//	* @_green, @_red, @_blue	->	colour 8-bit value
//*/
//void set_colour_one(uint8_t num_led, uint8_t _green, uint8_t _red, uint8_t _blue) {
//	//if ((num_led <= COUNT_LEDS) & ((DMA1->ISR & DMA_CCR_TCIE) != DMA_CCR_TCIE)) {
//	
//	while (dma_lock == 1) {}
//	dma_lock = 1;
//	if (num_led <= COUNT_LEDS) {
//		//while ((DMA1->ISR & DMA_CCR_TCIE) == DMA_CCR_TCIE);
//		NVIC_DisableIRQ(DMA1_Channel1_IRQn);
//		DMA1_Channel1->CCR &= (0<<0);

//		// set green 
//		for (uint8_t i = 0; i < 8; i++) {
//			uint8_t temp = (_green >> i) & 0x01;
//		
//	#ifdef RESET_BEFORE_DATA
//			if (temp == 0) {buf_leds[(7 - i) + (num_led) * ONE_LED_SIZE + RESET_SIZE] = ZERO_BIT;}
//			else {					buf_leds[(7 - i) + (num_led) * ONE_LED_SIZE + RESET_SIZE] = ONE_BIT;}
//	#endif
//			
//	#ifdef RESET_AFTER_DATA
//			if (temp == 0) {buf_leds[(7 - i) + (num_led) * ONE_LED_SIZE] = ZERO_BIT;}
//			else {					buf_leds[(7 - i) + (num_led) * ONE_LED_SIZE] = ONE_BIT;}
//	#endif
//		}
//		
//		// set red 
//		for (uint8_t i = 0; i < 8; i++) {
//			uint8_t temp = (_red >> i) & 0x01;
//			
//	#ifdef RESET_BEFORE_DATA
//			if (temp == 0) {buf_leds[(15 - i) + (num_led ) * ONE_LED_SIZE + RESET_SIZE] = ZERO_BIT;}
//			else {					buf_leds[(15 - i) + (num_led) * ONE_LED_SIZE + RESET_SIZE] = ONE_BIT;}
//	#endif
//			
//	#ifdef RESET_AFTER_DATA
//			if (temp == 0) {buf_leds[(15 - i) + (num_led) * ONE_LED_SIZE] = ZERO_BIT;}
//			else {					buf_leds[(15 - i) + (num_led) * ONE_LED_SIZE] = ONE_BIT;}
//	#endif
//		}
//		
//		// set blue 
//		for (uint8_t i = 0; i < 8; i++) {
//			uint8_t temp = (_blue >> i) & 0x01;
//			
//	#ifdef RESET_BEFORE_DATA
//			if (temp == 0) {buf_leds[(23 - i) + (num_led) * ONE_LED_SIZE + RESET_SIZE] = ZERO_BIT;}
//			else {					buf_leds[(23 - i) + (num_led) * ONE_LED_SIZE + RESET_SIZE] = ONE_BIT;}
//	#endif
//			
//	#ifdef RESET_AFTER_DATA
//			if (temp == 0) {buf_leds[(23 - i) + (num_led) * ONE_LED_SIZE] = ZERO_BIT;}
//			else {					buf_leds[(23 - i) + (num_led) * ONE_LED_SIZE] = ONE_BIT;}
//	#endif	
//		}
//		
//		 //buf_leds[BUF_LEDS_SIZE - 1] = PEPIOD + 1;
//		//buf_leds[BUF_LEDS_SIZE - 1] = 0;
//		
//		DMA1_Channel1->CPAR = (uint32_t) (&(TIM17->CCR1));
//		DMA1_Channel1->CMAR = (uint32_t)(&buf_leds[0]);
//		DMA1_Channel1->CNDTR = BUF_LEDS_SIZE;
//		//DMA1_Channel1->CNDTR = BUF_LEDS_SIZE + 1;
//		//DMA1_Channel1->CNDTR = BUF_LEDS_SIZE - 1;
//		
//		DMA1_Channel1->CCR |= DMA_CCR_DIR | DMA_CCR_MINC | 
//													DMA_CCR_TEIE | DMA_CCR_TCIE ;

//		DMA1_Channel1->CCR |= DMA_CCR_MSIZE_0;
//		DMA1_Channel1->CCR |= DMA_CCR_PSIZE_0;
//		
//		DMA1_Channel1->CCR |= DMA_CCR_EN;
//		
//		NVIC_EnableIRQ(DMA1_Channel1_IRQn);
//		NVIC_SetPriority(DMA1_Channel1_IRQn, 0);
//		
//	}
//}
//	
///*
//  * @st_led		-> 		start number of led in line
//  * @end_led	-> 		end number of led in line
//	* @_green, @_red, @_blue	->	colour 8-bit value
//*/
//void set_colour_range(uint8_t st_led, uint8_t end_led, uint8_t _green, uint8_t _red, uint8_t _blue) {
//	if ((st_led <= COUNT_LEDS) & (end_led <= COUNT_LEDS)) {
//		while (dma_lock == 1) {}
//		dma_lock = 1;
//		
//		DMA1_Channel1->CCR &= (0<<0);
//		NVIC_DisableIRQ(DMA1_Channel1_IRQn);

//		for (uint8_t i_led = st_led; i_led <= end_led; i_led++) {
//			// set green 
//			for (uint8_t i = 0; i < 8; i++) {
//				uint8_t temp = (_green >> i) & 0x01;
//			
//		#ifdef RESET_BEFORE_DATA
//				if (temp == 0) {buf_leds[(7 - i) + (i_led) * ONE_LED_SIZE + RESET_SIZE] = ZERO_BIT;}
//				else {					buf_leds[(7 - i) + (i_led) * ONE_LED_SIZE + RESET_SIZE] = ONE_BIT;}
//		#endif
//				
//		#ifdef RESET_AFTER_DATA
//				if (temp == 0) {buf_leds[(7 - i) + (i_led) * ONE_LED_SIZE] = ZERO_BIT;}
//				else {					buf_leds[(7 - i) + (i_led) * ONE_LED_SIZE] = ONE_BIT;}
//		#endif
//			}
//			
//			// set red 
//			for (uint8_t i = 0; i < 8; i++) {
//				uint8_t temp = (_red >> i) & 0x01;
//				
//		#ifdef RESET_BEFORE_DATA
//				if (temp == 0) {buf_leds[(15 - i) + (i_led) * ONE_LED_SIZE + RESET_SIZE] = ZERO_BIT;}
//				else {					buf_leds[(15 - i) + (i_led) * ONE_LED_SIZE + RESET_SIZE] = ONE_BIT;}
//		#endif
//				
//		#ifdef RESET_AFTER_DATA
//				if (temp == 0) {buf_leds[(15 - i) + (i_led) * ONE_LED_SIZE] = ZERO_BIT;}
//				else {					buf_leds[(15 - i) + (i_led) * ONE_LED_SIZE] = ONE_BIT;}
//		#endif
//			}
//			
//			// set blue 
//			for (uint8_t i = 0; i < 8; i++) {
//				uint8_t temp = (_blue >> i) & 0x01;
//				
//		#ifdef RESET_BEFORE_DATA
//				if (temp == 0) {buf_leds[(23 - i) + (i_led) * ONE_LED_SIZE + RESET_SIZE] = ZERO_BIT;}
//				else {					buf_leds[(23 - i) + (i_led) * ONE_LED_SIZE + RESET_SIZE] = ONE_BIT;}
//		#endif
//				
//		#ifdef RESET_AFTER_DATA		
//				if (temp == 0) {buf_leds[(23 - i) + (i_led) * ONE_LED_SIZE] = ZERO_BIT;}
//				else {					buf_leds[(23 - i) + (i_led) * ONE_LED_SIZE] = ONE_BIT;}		
//		#endif	
//			}
//		}
//		
//		//buf_leds[BUF_LEDS_SIZE - 1] = PEPIOD + 1;
//		//buf_leds[BUF_LEDS_SIZE - 1] = 0;
//		
//		DMA1_Channel1->CPAR = (uint32_t) (&(TIM17->CCR1));
//		DMA1_Channel1->CMAR = (uint32_t)(&buf_leds[0]); 
//		//DMA1_Channel1->CNDTR = BUF_LEDS_SIZE + 1; 
//		DMA1_Channel1->CNDTR = BUF_LEDS_SIZE; 
//		
//		DMA1_Channel1->CCR |= DMA_CCR_DIR | DMA_CCR_MINC | 
//													DMA_CCR_TEIE | DMA_CCR_TCIE ;

//		DMA1_Channel1->CCR |= DMA_CCR_MSIZE_0;
//		DMA1_Channel1->CCR |= DMA_CCR_PSIZE_0;
//		
//		NVIC_EnableIRQ(DMA1_Channel1_IRQn); 
//		NVIC_SetPriority(DMA1_Channel1_IRQn, 0);
//		
//		DMA1_Channel1->CCR |= DMA_CCR_EN;		
//	}
//}

//void DMA1_Channel1_IRQHandler (void) {
//	if ((DMA1->ISR & DMA_CCR_TCIE) == DMA_CCR_TCIE) { // Test if transfer completed on DMA channel 1
//		dma_lock = 0;
//		DMA1->IFCR |= DMA_IFCR_CTCIF1;
//	}
//}

#endif
